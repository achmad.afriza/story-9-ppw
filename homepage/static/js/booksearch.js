$("#searchform").submit(function(e) {
    e.preventDefault()

    var query = $(this).find(".statusform").val()
    if(query.length == 0) {
        $("#booktable").css("display", "none")
    }
    else {
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + query,
            success: function(data) {
                $("#booktable-content").empty()
                if(data.totalItems == 0) {
                    $("#booktable").css("display", "none")
                }
                else {
                    $("#booktable").css("display", "table")
                    var content = ""
                    for(var i = 0; i < data.items.length; i++) {
                        content += "<tr>"
                        
                        var Identifier
                        try {
                            Identifier = data.items[i].volumeInfo.industryIdentifiers[0].identifier
                        } catch(TypeError) {
                            Identifier = undefined
                        }
                        finally {
                            content += `<td class='align-middle'>${Identifier}</td>`
                        }

                        var Id = data.items[i].id
                        var Cover = `<img src='${data.items[i].volumeInfo.imageLinks.thumbnail}'></img>`
                        var Title = data.items[i].volumeInfo.title
                        var Author = data.items[i].volumeInfo.authors
                        var Publisher = data.items[i].volumeInfo.publisher
                        var PDate = data.items[i].volumeInfo.publishedDate
                        content += `<td class='align-middle'>${Cover}</td>`
                        content += `<td class='align-middle'>${Title}</td>`
                        content += `<td class='align-middle'>${Author}</td>`
                        content += `<td class='align-middle'>${Publisher}</td>`
                        content += `<td class='align-middle'>${PDate}</td>`

                        $.ajax({
                            type: 'GET',
                            url: LikeAPI,
                            data: {'ID': Id},
                            success: function(response) {
                                var book = JSON.parse(response['response'])
                                console.log(book)
                                // content += `<td class='align-middle review-number'>${likes}</td>`
                            }
                        })
                        content += "<td class='align-middle'>"
                        + `<form method="POST" action="${LikeAPI}">
                            <input type="hidden" name="ID" value="${Id}">
                            <label>
                                <input type="submit" style="display: none;">
                                <svg type="submit" class="icons hvr-pop button-like" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="white" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5 1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-2z"/></svg>
                            </label>
                        </form>`
                        + "</td>"
                        content += "</tr>"

                        /**
                         * <input type="hidden" name="Identifier" value="${Identifier}">
                            <input type="hidden" name="Cover" value="${Cover}">
                            <input type="hidden" name="Title" value="${Title}">
                            <input type="hidden" name="Author" value="${Author}">
                            <input type="hidden" name="Publisher" value="${Publisher}">
                            <input type="hidden" name="PDate" value="${PDate}">
                         */
                    }
                    
                    $("#booktable-content").append(content)
                }
            },
        })
    }
})