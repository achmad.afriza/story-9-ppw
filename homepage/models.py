from django.db import models

# Create your models here.
class Book(models.Model):
    ID = models.CharField(primary_key=True, max_length=100)
    # Identifier = models.CharField()
    # Cover = models.CharField()
    # Title = models.CharField()
    # Author = models.CharField()
    # Publisher = models.CharField()
    # PDate = models.CharField()
    Likes = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f"{self.ID}"