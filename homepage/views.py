from django.shortcuts import render, redirect
from django.contrib import messages
from django.core import validators, serializers
from django.urls import reverse
from django.http import JsonResponse

from .forms import LikeBook
from .models import Book

# Create your views here.
def index(request):
    context = {
        'nav': [
            [reverse('homepage:index'), 'Home'],
            ['https://story5ppw-achmadafriza.herokuapp.com/', 'About Me'],
        ]
    }
    return render(request, 'homepage.html', context)

def countLike(request):
    if request.is_ajax and request.method == "GET":
        try:
            book = Book.objects.get(ID=request.GET.get('ID'))

            obj = serializers.serialize('json', [book, ])
            return JsonResponse({"valid": True, "response": obj}, status=200)
        except Book.DoesNotExist:
            return JsonResponse({"valid": False}, status=200)
        
        # some error occured
        return JsonResponse({"error": ""}, status=400)

    elif request.is_ajax and request.method == "POST":
        try:
            book = Book.objects.get(ID=request.POST.get('ID'))
            book.Likes += 1
            book.save()

            obj = serializers.serialize('json', [book, ])
            return JsonResponse({"response": obj}, status=200)
        except Book.DoesNotExist:
            form = LikeBook(request.POST)
            if form.is_valid():
                form.save()

                book = Book.objects.get(ID=request.POST.get('ID'))
                obj = serializers.serialize('json', [book, ])
                return JsonResponse({"response": obj}, status=200)
            else:
                # some form errors occured.
                return JsonResponse({"error": form.errors}, status=400)
    
    # some error occured
    return JsonResponse({"error": ""}, status=400)

def topRated(request):
    bookCount = Book.objects.count()
    cutoff = Book.objects.order_by('-Likes')[min(5, bookCount)-1].Likes
    books = Book.objects.filter(Likes__gte=cutoff).order_by('-Likes').values()

    return JsonResponse({"response": list(books), 'count': bookCount-1}, status=200)